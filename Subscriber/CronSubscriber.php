<?php

namespace HrCheck24\Subscriber;

use Enlight\Event\SubscriberInterface;
use HrCheck24\Service\Check24Service;
use Shopware_Components_Cron_CronJob;

class CronSubscriber implements SubscriberInterface
{
    private Check24Service $check24Service;

    public function __construct(Check24Service $check24Service)
    {
        $this->check24Service = $check24Service;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_CronJob_HrCheck24Cron' => 'onCheck24Cron'
        ];
    }

    /**
     * @param Shopware_Components_Cron_CronJob $job
     *
     * @return string
     */
    public function onCheck24Cron(Shopware_Components_Cron_CronJob $job): string
    {
        return $this->check24Service->call();
    }

}
