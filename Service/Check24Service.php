<?php

namespace HrCheck24\Service;

use GuzzleHttp\Exception\GuzzleException;
use Shopware\Components\HttpClient\GuzzleFactory;

class Check24Service
{
    private $client;

    private array $pluginConfig;

    public function __construct(GuzzleFactory $factory, $pluginConfig)
    {
        $this->client = $factory->createClient();
        $this->pluginConfig = $pluginConfig;
    }

    /**
     * @return string
     * @throws GuzzleException
     */
    public function call(): string
    {
        $result = $this->client->get($this->pluginConfig['triggerUrl']);
        if ($result->getStatusCode() === 200) {
            return "Import abgeschlossen";
        }

        return "Etwas ist schief gelaufen!";
    }
}
